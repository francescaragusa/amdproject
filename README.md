# AMD project: birds clustering

This project was developed using a Jupyter notebook with Python 3.6 kernel.

## Repository description

This repository is structured as follows:
```
.
├── code
│   ├── bfr.py
│   ├── bfr_tuning.ipynb
│   ├── eval.py
│   ├── kmeans.py
│   ├── kMeans_tuning.ipynb
│   ├── preprocessing.ipynb
│   └── util.py
│   ├── files
│   │   ├── birds_v35.tar.xz
│   │   ├── data_reduced
│   │   ├── exp1_bfr
│   │   ├── exp2_bfr
│   │   ├── kmeans_tuning
│   │   ├── kmeans_vs_kmeanspp
│   │   └── mt_ut_tuning
├── README.md
└── report.pdf
```

In the `code` folder are contained:
- all the developed code:
  - `preprocessing.ipynb` contains the preprocessing technique applied on the dataset
  - `kmeans.py` and `bfr.py` contain an implementation of the homonyms algorithms
  - `eval.py` and `util.py` contain some useful functions
  -  `kMeans_tuning.ipynb` and `bfr_tuning.ipynb` contain the experiments performed on the algorithms
- a `files` folder containing some dump files and an archive. The dump files usually contain useful data structures/test results obtained while running the notebooks

## Addressing compatibility issues

To avoid any compatibility issues due to possible updates of the dataset, I decided to include in `code/files` an archive containing the `consolidated` folder of the version n.35 of the dataset. Moreover, in the same folder, I stored in `data_reduced` the processed data obtained with the pre processing technique applied in `preprocessing.ipynb`.

The experiments in `kMeans_tuning.ipynb` and `bfr_tuning.ipynb` were conducted using `data_reduced` and the content of the archive.

## Step by step execution

The first file to be executed should be `preprocessing.ipynb`. Note that at the beginning of the file there's the following command to extract the archive in order to use the version n.35 of the dataset:
```shell
!tar -xf files/birds_v35.tar.xz
```

However, I decided to also include the portion of code capable of downloading the dataset from Kaggle at runtime (even if commented).
At the end of the file, I used `pickle` to make a dump file called `data_reduced` of the processed data and stored it in the `files` folder.

Now, it's time to execute `kMeans_tuning.ipynb` and `bfr_tuning.ipynb`. Both files make use of `data_reduced`. In fact, at the beginning of the files there is the following command:

```python
with open ('files/data_reduced', 'rb') as fp:
    data = pickle.load(fp)
```

Moreover, before the clustering inspection, there's the command to extract the archive containing the dataset. Note that, if you have already decompressed the archive, there's no need to do it again.

Note that you can run both `kMeans_tuning.ipynb` and `bfr_tuning.ipynb` without running first `preprocessing.ipynb`, since all the necessary files are stored in `files`.
