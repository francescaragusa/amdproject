import numpy as np
import multiprocessing
import random
from scipy.spatial import distance
from math import sqrt
from itertools import repeat
from sklearn.cluster import AgglomerativeClustering
import pandas as pd
import util
import time
import sys

random.seed(140)
np.random.seed(140)




class bfr:

  # -------- INIT --------
  '''
  Init function
  
  data              : dataset (all fields)
  merge_threshold   : parameter (merging threshold for mini clusters)
  upgrade_threshold : parameter (upgrade threshold for mini clusters)
  '''
  def __init__(self, data, merge_threshold, upgrade_threshold):
    # parameters
    self.verbose = True
    self.mt = merge_threshold
    self.ut = upgrade_threshold
    self.tau= 1
    # summary
    self.dimensions = len(data[0]['feat'])
    self.dtype = [('id', np.int32),
                  ('n', np.int32),
                  ('sum', (np.ndarray , self.dimensions)),
                  ('sumsq', (np.ndarray , self.dimensions)),
                  ('centroid', (np.ndarray , self.dimensions)),
                  ('var', (np.ndarray , self.dimensions))]
    # data structures
    self.discard_set = np.zeros(0, dtype= self.dtype)
    self.compressed_set = np.zeros(0, dtype= self.dtype)
    self.retained_set = np.empty(0, dtype=np.int32)
    # index mapping 
    self.id = 0
    self.id_map = {}
    # stats
    self.n_discard = [0]
    self.n_compress = [0]
    self.n_retain = [0]



  # -------- FUNCTIONS  --------
  '''
  Summary creation of a new cluster. The summary will have a brand-new id.

  data   : dataset (all fields)
  points : set of points contained in the cluster (all fields)
  '''
  def create_cluster(self, data, points):
    new_cls = np.zeros(1, dtype= self.dtype)
    new_cls['id'] = self.id 
    self.id += 1
    # cluster creation
    new_cls['n'] = len(points)
    new_cls['sum'] = np.sum(data[points]['feat'], axis = 0)
    new_cls['sumsq'] = np.sum(np.square(data[points]['feat']), axis = 0)
    new_cls['centroid'] = new_cls['sum']/new_cls['n'].reshape(-1,1)
    new_cls['var'] = self.variance(new_cls)[0]
    # update of the clustering
    for p in points:    
      data[p]['cluster'] = new_cls['id'][0]
    # index mapping update
    self.id_map[self.id - 1] = [self.id - 1]
    return new_cls


  '''
  Insertion of a point into an existing cluster
  
  data  : dataset (all fields)
  cls   : cluster index
  point : point (all fields)
  '''
  def add_to_cluster(self, data, cls, point):
    self.discard_set[cls]['n'] += 1 
    self.discard_set[cls]['sum'] += data[point]['feat'][0]
    self.discard_set[cls]['sumsq'] +=  np.square(data[point]['feat'][0])
    self.discard_set[cls]['centroid'] = self.centroid( self.discard_set[cls])
    self.discard_set[cls]['var'] = self.variance(self.discard_set[cls])[0]
    data[point[0]]['cluster'] = self.discard_set[cls]['id']



  '''
  Return the variance of a cluster c
  '''
  def variance(self, c):
    var = (c['sumsq']/c['n'].reshape(-1,1) )  - np.square( c['sum']/c['n'].reshape(-1,1) )
    var[var == 0] = 0.0001
    return var 



  '''
  Return the centroid of a cluster c
  '''
  def centroid(self,c):
    return (c['sum']/c['n'].reshape(-1,1)).flatten()




  '''
  Return the Mahalanobis distance between a point and a cluster/set of clusters
  
  a : point (feature only)
  b : centroid/s of the cluster/s
  d : variance/s of the cluster/s
  '''
  def mahalanobis(self, a, b, d):
    t = zip(repeat(a),b,d)
    pool = multiprocessing.Pool()
    dist = pool.starmap(distance.seuclidean, t)
    pool.close()
    pool.join()
    return np.array(dist)




  '''
  Try to insert a point to the closest cluster. Return true if successful, false otherwise

  data  : dataset (all fields)
  point : point (all fields)
  check : if TRUE, always add the point
  '''
  def try_add_to_cluster(self, data, point, check=False):
    var = self.discard_set['var']
    p = data[point]['feat']
    centroids = self.discard_set['centroid']
    index_nearest = np.argmin( distance.cdist(p , centroids, 'euclidean' ) ) 
    mal_dist = distance.seuclidean(p, centroids[index_nearest].reshape(1,-1), var[index_nearest].reshape(1,-1)) 
    if check or mal_dist < self.tau* sqrt(self.dimensions): 
      self.add_to_cluster(data, index_nearest, point)
      return True
    return False




  '''
  Try to insert a point into a cluster. In case of faiure, insert the poin in the retained set
  
  data  : dataset (all fields)
  point : point (all fields)
  '''
  def process_point(self, data, point):
    # provo ad inserire il punto in un cluster esistente
    if not self.try_add_to_cluster(data,point):
      # altrimenti lo salvo per dopo
      self.retained_set = np.append(self.retained_set, point)




  '''
  Mini clusters creation function. Each new mini cluster must have at least 3 points
  
  data  : dataset (all fields)
  '''
  def create_new_miniclusters(self,data):
    if len(self.retained_set) > 1:
      points = data[self.retained_set]['feat']
      # hierarchical clustering
      clustering = AgglomerativeClustering(n_clusters=None, affinity='euclidean', linkage='ward', compute_full_tree = True, distance_threshold = 4.5).fit(points)
      h_clustering = clustering.labels_
      unique, counts = np.unique(h_clustering, return_counts=True)
      # select only the clusters with at least 3 elements
      h_centroids = unique[np.where(counts > 2)[0]].reshape(-1,1)
      to_keep = [elem not in h_centroids for elem in h_clustering]
      if len(h_centroids) > 0:
        new_mini_clusters = np.apply_along_axis(lambda c : self.create_cluster(data, self.retained_set[h_clustering == c]), 1, h_centroids)
        self.compressed_set = np.append(self.compressed_set, new_mini_clusters)
        self.retained_set = np.compress(to_keep, self.retained_set)
        

      

  '''
  Try to merge two mini clusters. If it happens, it updates the first and delete the second

  data : dataset (all fields)
  mc1  : index of the first mini cluster
  mc2  : index of the second mini cluster
  '''
  def try_merge_miniclusters(self, data, mc1, mc2):
    # variance of mc1 and mc2
    v1 = self.compressed_set[mc1]['var']
    v2 = self.compressed_set[mc2]['var']
    dump = self.compressed_set[mc1].copy()
    # temporary merge
    self.compressed_set[mc1]['n'] +=  self.compressed_set[mc2]['n']
    self.compressed_set[mc1]['sum'] += self.compressed_set[mc2]['sum']
    self.compressed_set[mc1]['sumsq'] +=  self.compressed_set[mc2]['sumsq']
    self.compressed_set[mc1]['centroid'] =  self.centroid(self.compressed_set[mc1])
    self.compressed_set[mc1]['var'] = self.variance(self.compressed_set[mc1])[0]
    # combined variance
    v_new = self.compressed_set[mc1]['var']
    if (all(v_new < self.mt * v1) or all(v_new < self.mt * v2)):
      id1 =  self.compressed_set[mc1]['id']
      id2 =  self.compressed_set[mc2]['id']
      self.id_map[id1].extend(self.id_map[id2]) 
      del self.id_map[id2]
      # update compressed set
      self.compressed_set = np.delete(self.compressed_set, mc2, axis = 0) 
      del dump
    else:
      # rollback
      self.compressed_set[mc1] = dump.copy()
    



  '''
  Try to promote a mini cluster to cluster. If it happens, then it returns the index of the cluster. Otherwise it returns -1

  mc      : mini cluster index
  n_mean  : average number of points in clusters 
  '''
  def try_upgrade_to_cluster(self, mc, n_mean):
    if self.compressed_set[mc]['n'] > self.ut * n_mean :
      self.discard_set = np.append(self.discard_set, self.compressed_set[mc])
      return mc
    return -1




  '''
  Tasks:
    1. merge together mini clusters that are close enough
    2. upgrade mini clustest that are large enough
  
  data : dataset (all fields)
  '''
  def update_and_upgrade_compressed(self,data):
    # merging (attempt)
    new_compressed = np.zeros(0, dtype= self.dtype)
    self.n_compress.append(len(self.compressed_set))
    if self.verbose:
      print('len compressed_set before merging procedure:', len(self.compressed_set))
    np.random.shuffle(self.compressed_set)

    while len(self.compressed_set) > 1:
      cls =  self.compressed_set[0]['centroid']
      dist = self.mahalanobis(cls.reshape(1,-1), self.compressed_set[1:]['centroid'], self.compressed_set[1:]['var']).flatten()
      closer = np.amin(dist)
      if closer <= 4 * sqrt(self.dimensions):
        self.try_merge_miniclusters(data, 0, np.argmin(dist)+1) #+1 perche non conto lo zero
      new_compressed = np.append(new_compressed, self.compressed_set[0].copy())
      self.compressed_set = np.delete(self.compressed_set, 0)
    if (len(self.compressed_set) == 1):
      new_compressed = np.append(new_compressed, self.compressed_set[0].copy())
    self.compressed_set = new_compressed.copy()    

    self.n_compress.append(len(self.compressed_set))
    if self.verbose:
      print('len compressed_set after merging procedure:', len(self.compressed_set))    
    
    # upgrade mini clusters
    if len(self.discard_set) > 0: #only first iteration of the algorithm, when the discard set is empty
      n_mean = np.mean(self.discard_set['n'])
    else:
      n_mean = 0
    cs = len(self.compressed_set)
    if cs > 0:
      to_delete = list(map(lambda mc: self.try_upgrade_to_cluster(mc, n_mean) ,  np.arange(cs)))
      if len(to_delete) > 0 :
        to_delete = list(filter(lambda x : x != -1, to_delete))
        self.compressed_set = np.delete(self.compressed_set, to_delete, axis = 0)

    self.n_compress.append(len(self.compressed_set))
    self.n_discard.append(len(self.discard_set))
    if self.verbose:
      print('len compressed_set after upgrade procedure:', len(self.compressed_set))    
      print('#clusters:', len(self.discard_set))
    # exit conditions
    if len(self.discard_set) > 500:
        sys.exit('Stopped: #clusters > 500')
    if time.time() - self.start > 3600:
        sys.exit('Stopped: timeout execution')




  '''
  Merge together the given cluster with the closest cluster 
  
  data        : dataset (all fields)
  minicluster : minicluster (all fields)
  '''
  def merge_minicluster_cluster(self, data, minicluster):
    centroid = minicluster['centroid']
    discard_centroids = self.discard_set['centroid']
    index_nearest = np.argmin(self.mahalanobis(centroid, discard_centroids, self.discard_set['var'] ) )
    # update discard set
    self.discard_set[index_nearest]['n'] +=  minicluster['n']
    self.discard_set[index_nearest]['sum'] += minicluster[0]['sum']
    self.discard_set[index_nearest]['sumsq'] +=  minicluster[0]['sumsq']
    self.discard_set[index_nearest]['centroid'] =  self.centroid(self.discard_set[index_nearest])
    self.discard_set[index_nearest]['var'] = self.variance(self.discard_set[index_nearest])[0]
    # update index mapping
    id1 =  self.discard_set[index_nearest]['id']
    id2 =  minicluster['id'][0]
    self.id_map[id1].extend(self.id_map[id2]) 
    del self.id_map[id2]



  '''
  Tasks:
    1. Process each point of a chunk one by one
    2. Try to create new mini clusters
    3. Update of the compressed set
  
  data  : dataset (all fields)
  chunk : indexes of the points to be processed
  '''
  def process_chunk(self, data, chunk):
    if self.tau<= 4:
      self.tau= self.tau+ 0.1
    if self.verbose:
      print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
      print('processing points:', chunk[0], 'to' ,chunk[-1])
    np.apply_along_axis(lambda x : self.process_point(data, x), 1, chunk)
    # creation of new mini clusters
    if self.verbose:
      print('len retain set before hc:', len(self.retained_set))
    self.n_retain.append(len(self.retained_set))
    self.create_new_miniclusters(data)
    self.n_retain.append(len(self.retained_set))
    if self.verbose:
      print('len retain set after hc:', len(self.retained_set))
    # upgrade/update of mini clusters
    if len(self.compressed_set) > 1:
      self.update_and_upgrade_compressed(data)





  # -------- FIT --------
  '''
  Fit function
  
  data       : dataset (all fields)
  chunk_size : chunk size
  verbose    : print a summary of the state at each iteration (optional)
  '''
  def fit(self, data, chunk_size, verbose=True):
    self.verbose = verbose
    chunk_generator = util.chunks(len(data),chunk_size)
    # init of the discard set with the first chunk
    c = next(chunk_generator).flatten()
    self.retained_set = np.append(self.retained_set, c)
    if self.verbose:
      print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
      print('processing points:', c[0], 'to' , c[-1])
      print('len retain set before hc:', len(self.retained_set))
    self.n_retain.append(len(self.retained_set))
    self.create_new_miniclusters(data)
    if self.verbose:
      print('len retain set after hc:', len(self.retained_set))
    self.n_retain.append(len(self.retained_set))
    self.update_and_upgrade_compressed(data)

    # process of the other chunks
    for chunk in chunk_generator:
      self.process_chunk(data,chunk)

    # assignment of minicluster & isolated points 
    if len(self.retained_set) > 0:
      np.apply_along_axis(lambda p : self.try_add_to_cluster(data,p,True), 1, self.retained_set.reshape(-1,1))
    if len(self.compressed_set) > 0:
      np.apply_along_axis(lambda mc : self.merge_minicluster_cluster(data, mc), 1, self.compressed_set.reshape(-1,1))
    del self.compressed_set
    del self.retained_set
    
    # update index mapping
    new_map = dict()
    len_dis = len(self.discard_set)
    for pos in range(len_dis):
      for val in self.id_map[self.discard_set[pos]['id']]:
        new_map[val] = pos
      self.discard_set[pos]['id'] = pos
    self.id_map = new_map
    clusters = pd.DataFrame(data['cluster'])
    clusters.replace(self.id_map, inplace=True)
    data['cluster'] = clusters.to_numpy().flatten()
    # centroids  
    self.centroids = self.discard_set['centroid']
    self.ids =  self.discard_set['id']
    del self.discard_set