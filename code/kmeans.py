import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
import util


class kMeans:

  #-------- INIT --------
  '''
  Init function

  data     : dataset 
  k        : number of clusters
  max_it   : max iteration threshold
  init_c   : init function for choosing the centroids
  *a, **ka : arguments of init_c
  '''
  def __init__(self, data ,k, max_it ,init_c, *a, **ka):
    self.k = k
    self.max_it = max_it
    self.current_it = 0
    self.indexes = np.arange(k).reshape(k,1)
    self.centroids = data['feat'][init_c(*a,**ka)]
    self.conv = False


  # -------- CLUSTERS MANIPULATION --------

  '''
  Assign the closest centroid to each point 
  '''
  def compute_clusters(self, dataset):
    new_clusters = np.argmin(euclidean_distances( dataset['feat'], self.centroids), 1)
    self.conv = np.array_equal(new_clusters, dataset['cluster'])
    dataset['cluster'] = new_clusters

  # -------- CENTROIDS MANIPULATION --------

  '''
  Return the centroid of the k-th cluster
  '''
  def calculate_new_center(self, dataset, k):
    points = util.get_points_of_cluster(dataset['feat'], dataset['cluster'],  k)
    return np.mean(points, axis=0)

  '''
  Centroids update function (recompute the centroids of each cluster)
  '''
  def update_centroids(self, dataset):
    self.centroids = np.apply_along_axis(lambda k : self.calculate_new_center(dataset, k), 1, self.indexes)

  # -------- FIT --------
  '''
  Fit function
  '''
  def fit(self, dataset, verbose=True):
    while (not self.conv) and (self.max_it - self.current_it ) > 0:
      self.current_it += 1
      self.compute_clusters(dataset)
      self.update_centroids(dataset)
    if verbose:
      if self.conv:
        print("Convergence achieved")
      else:
        print("Max iteration reached")