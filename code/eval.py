import numpy as np
import math
from sklearn.metrics.pairwise import euclidean_distances
import util


''' 
Return the SSE value of a single cluster

feat       : features vectors of the dataset
clustering : clustering
centroids  : centroids of the clustering
k          : cluster index
'''
def single_sse(feat, clustering, centroid ,k):
  cluster_k = util.get_points_of_cluster(feat, clustering, k)
  return np.sum(np.square(euclidean_distances(cluster_k, centroid.reshape(1, -1))))


''' 
Return the SSE value of a clustering

feat       : features vectors of the dataset
clustering : clustering
centroids  : centroids of the clustering
'''
def sse(feat, clustering, centroids):
  indexes = np.arange(len(centroids)).reshape(-1,1)
  return np.sum(np.apply_along_axis(lambda k: single_sse(feat, clustering, centroids[k], k), 1, indexes))





''' 
Return the entropy value of a single cluster

label      : label vectors of the dataset
clustering : clustering
k          : cluster index
'''
def single_entropy(label, clustering, k):
  cluster_k = util.get_points_of_cluster(label, clustering, k)
  labels = list(map( lambda x : util.get_species(str(x)), cluster_k) )
  # fracs = wl/nw
  _ , fracs = np.unique(labels, return_counts=True)
  fracs = fracs/len(labels)
  fracs = np.array(fracs).reshape(len(fracs),1)
  return - np.sum(np.apply_along_axis(lambda x: x * (math.log(x,2)), 1, fracs))


'''
Return the entropy value of a clustering

label      : label vectors of the dataset
clustering : clustering
'''
def entropy(label, clustering):
  k , nw  = np.unique(clustering, return_counts=True)
  nw = np.array(nw).reshape(len(nw),1)
  k = np.array(k).reshape(len(k),1)
  n = len(clustering)
  return np.sum( np.apply_along_axis(lambda x: single_entropy(label,clustering,x)*nw[x]/n  , 1, k) )