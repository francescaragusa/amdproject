import numpy as np
import random
from sklearn.metrics.pairwise import euclidean_distances
from scipy import stats
import matplotlib.pyplot as plt
from keras.preprocessing.image import load_img 
import ntpath

random.seed(140)
np.random.seed(140)


'''
Uniform extraction of k centroids (indeces in the dataset)

dataset : dataset
k       : number of centroids
'''
def extract_rand_centroids(dataset,k):
  return random.sample(range(len(dataset)), k)





'''
Extraction of k centroids using kmeans++

dataset : dataset
k       : number of centroids
'''
def kmeans_pp(dataset,k):
  selected = random.sample(range(len(dataset)), 1)
  points = np.arange(len(dataset))
  while len(selected) < k:
    probs = np.square(np.amin(euclidean_distances( dataset, dataset[selected]), 1))
    probs = probs / np.sum(probs)
    distribution = stats.rv_discrete(values=(points, probs), seed= 140)
    selected.append(distribution.rvs(size=1)[0])
  return selected





'''
Return the poinst of the k^th cluster

data : dataset
k    : cluster index
'''
def get_points_of_cluster(data, clustering ,k):
  return data[np.nonzero(clustering == k)]





'''
Return the name species given a filepath

label : filepath
'''
def get_species(label):
  species, _ = ntpath.split(label)
  return ntpath.basename(species)





'''
Print (at most) 30 images of the given cluster. The images are chosen evenly

data : dataset
k    : cluster index
'''
def images_of_kcluster(data,k):
  plt.figure(figsize = (25,25));
  files = get_points_of_cluster(data['label'], data['cluster'],k)
  # limita a 30 immagini
  if len(files) > 30:
      print(f"Showing only 30 of {len(files)} images")
      files = files[random.sample(range(len(files)), 30)]
  # stampa
  for index, file in enumerate(files):
      plt.subplot(10,10,index+1);
      img = load_img(file)
      img = np.array(img)
      plt.imshow(img)
      plt.axis('off')





'''
Chunk generator of size n
'''
def chunks(limit, n):
  for i in range(0, limit, n):
    if i+n < limit: 
      yield np.array(range(i,i+n,1)).reshape(-1,1)
    else:
      yield np.array(range(i,limit,1)).reshape(-1,1)